# School Tag Particle Station

Gamifying the school commute using NFC + IoT.  Students use a backpack NFC tag to scan into a station that will send the tag's uid to the cloud to earn rewards for biking, walking or taking the bus.  

The particle stations uses Particle Mesh products (generation 3) Boron,Argon,Xenon.  Connected to an NFC reader that reads the tag, the cloud receives the published message and passes it on to the School Tag cloud for processing.

![Architecture diagram](https://docs.google.com/drawings/d/e/2PACX-1vSHtkzSRXbaVtr-CD5gNmu8eCckYVeeKRkLc-2KMG-ZK4dVY1uxMhHDjRWMyP9qc_pUmaBgYu5TSoA4/pub?w=1102&amp;h=713)

## Build and Deploy

All code is found in the school-tag-station-particle.ino file.  You can build and publish it to a device using Particle Workbench.

## Remote Station Prototype Assembly

The prototype stations are mounted to poles around town using zip ties or screws into wooden posts.  They are not weather proof and are unlikely to survive a real rainstorm so future enhancements must improve the form.

![Photo of Station](https://lh3.googleusercontent.com/w9TG9tyyg7QfJPOwxQw_oCbZGb4oKkfzsajQC6FfqP9E5PO1ZvRbmu-zWW1qh0LIOesoz-4Cg3lyuerWgSQX6hKqfFN9Jetff0AxaHTFi_J0daga8D0gwbjnppibGYGurFYDNVOaeo9Zew6a_WwV1B1-Uz-Wuqw_X2JNOOERgBxw1BZ5dajENjGZFh4x3_nN88Bvt-XGaZgZAR1hzBasK2B8nryIRWU0dnyObLLe1VnhV9rG1dH_z4Xtz_UQFHtpvBlEK74C_0saYqdP2KTRpSB4GLRL6sZFBPtG5vOj78xb6Py5i0821JCFXjs2uS1qi63Exe3bkiL2hEaN-N48lxCfl0mINX_V6VecFPLbY4uh5fXBN7u0XK-C_4FD3Zwe41AtcPiWy0_no3ZtWUCZWGVU_DUKExxg_WZqsu_ykaW-K1vH1SZ6sEOGPBgnWfUCiIu2p1RJ7tUmPXC_YLDA-b1t5VqAURNoIhTH3hCdwHqjI0MEebH28tKnoveChSirCP9FHSmlE2QXiXY2y5j8Ftc3H96GqOe84KOkh3XsxJwv6YqKz2iXOS0rvveZ0x9h1PgeX8o4VJoDA6aS0_IJ2kBkF-2gWEZT5SU_aXXvMqYyfqCZZKtuXXdCEUxCLKPoFCEjeVNt9K7Sv22Wune8qi0TGzTukq0AtfTklDr-p-rpMMoQRR3i_3tvzyAV2c0ivtKwl0MDpImoG_lDYDKYRGAg7A=w720-h1280-no)

![Back of box](https://lh3.googleusercontent.com/VOhQSOGctKsTHiL94Z-UVIUk8J_HLhNYp9KAXxKvz2MIHIxy0nuLf-Ei0D7mwGMcWgMwVk4z_Jch1dDZmp-jFdZQwRTYOowjVTxHuwFcXV9z39SnWz-_iP7xgp-Sk2sk2xB8zZxT4C2DzJtys9IjGvBmp5-LlC88o9crdT5kNKABrWhbgMhwL5aLzuCHCxyNWSubIJLAPTjhgcMFxueAlrKZFtvCul2EsZsH8ueyeC0Ij_IyfJIT4O1WCUjRbBEUP7PC3klNozDUHiMGWFakRIebvzbfoKeACf4OPaawnYW7T5rks0whyuX2yWWI_oufsADjCH5FONYdhyfUP00pq9XuSNKo1sgO3rn_UGuew8IwsVe2-RCeaPG3moCTV_Db5KsrBITzSRJiSLu_092ePViX965GxLK8N1fDIfcDnplGNT5o9T0_j5OjqpN4mFDxnljl1XTjgwnv80ynKoQ2ThseR15ZdiPyp6O5vftcNGanIcC2WuESKX-_pFG661MRdvLwFh8ydkkZcNm2qfogQfaK_wiDDu5vdxk3hbE9TCmhVb0oco2W4JkgoYKA5PnmBctrGGh6tgeQfOOcYJdwNFCcDS_Jq7jkHyVnDHcFuQzQKaLjglYBZSs92bXKDyBuwg9ryRneW7mZkDxzNm3mCkkkoFhjvB4BD8uVF6BkBptyjaDVscl1nS7tmsr_dVsE0a2Whtde91XnjnhZvkisvx4sag=w720-h1280-no)

* [Sanitor Bead Storage](https://www.amazon.com/gp/product/B07KVRHFG7/ref=ppx_yo_dt_b_asin_title_o09_s01?ie=UTF8&psc=1) as the container
* [Nylon Standoffs](https://www.amazon.com/gp/product/B076CMBKPL/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1) to mount the boards
* [Jumper wires](https://www.amazon.com/gp/product/B07GD312VG/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1) to connect the boards
* [Mifare 522 RFID Boards](https://www.amazon.com/gp/product/B00UBE1XJW/ref=ppx_yo_dt_b_asin_title_o03_s00?ie=UTF8&psc=1) to read the NFC Tags
* Particle [Argon](https://docs.particle.io/argon/) or [Xenon](https://docs.particle.io/xenon) for the brains and connectivity
* [NFC Tags - any NTAG 213](https://store.gototags.com/nfc/nfc-tags/) to identify the player
* [Battery](https://store.particle.io/products/li-po-battery) to power the station
* Hot Glue Gun to mount parts

See [ino file](./school-tag-station-particle.ino) for pin connection.

## Welcome to your Particle project!

Every new Particle project is composed of 3 important elements that you'll see have been created in your project directory for school-tag-station-particle.

#### ```/src``` folder:  
This is the source folder that contains the firmware files for your project. It should *not* be renamed. 
Anything that is in this folder when you compile your project will be sent to our compile service and compiled into a firmware binary for the Particle device that you have targeted.

If your application contains multiple files, they should all be included in the `src` folder. If your firmware depends on Particle libraries, those dependencies are specified in the `project.properties` file referenced below.

#### ```.ino``` file:
This file is the firmware that will run as the primary application on your Particle device. It contains a `setup()` and `loop()` function, and can be written in Wiring or C/C++. For more information about using the Particle firmware API to create firmware for your Particle device, refer to the [Firmware Reference](https://docs.particle.io/reference/firmware/) section of the Particle documentation.

#### ```project.properties``` file:  
This is the file that specifies the name and version number of the libraries that your project depends on. Dependencies are added automatically to your `project.properties` file when you add a library to a project using the `particle library add` command in the CLI or add a library in the Desktop IDE.

## Adding additional files to your project

#### Projects with multiple sources
If you would like add additional files to your application, they should be added to the `/src` folder. All files in the `/src` folder will be sent to the Particle Cloud to produce a compiled binary.

#### Projects with external libraries
If your project includes a library that has not been registered in the Particle libraries system, you should create a new folder named `/lib/<libraryname>/src` under `/<project dir>` and add the `.h`, `.cpp` & `library.properties` files for your library there. Read the [Firmware Libraries guide](https://docs.particle.io/guide/tools-and-features/libraries/) for more details on how to develop libraries. Note that all contents of the `/lib` folder and subfolders will also be sent to the Cloud for compilation.

## Compiling your project

When you're ready to compile your project, make sure you have the correct Particle device target selected and run `particle compile <platform>` in the CLI or click the Compile button in the Desktop IDE. The following files in your project folder will be sent to the compile service:

- Everything in the `/src` folder, including your `.ino` application file
- The `project.properties` file for your project
- Any libraries stored under `lib/<libraryname>/src`
