
#include <MFRC522.h>
#include <PublishQueueAsyncRK.h>
#include <TagIo.h>
#include <StationSound.h>
#include "MifareUltralight.h"

using namespace ndef_mfrc522;

/*
 *
 MFRC522 - Library to use ARDUINO RFID MODULE KIT 13.56 MHZ WITH TAGS SPI W AND
 R BY COOQROBOT.
 * The library file MFRC522.h has a wealth of useful info. Please read it.
 * The functions are documented in MFRC522.cpp.
 *---------------------------------------------------------------------------
 * Pin layout should be as follows:
 * Signal     Pin              Pin               Pin			 Wire
 Color (Optional)
 *            Arduino Uno      Particle Mesh     MFRC522 board
 * ---------------------------------------------------------------------------
 * Reset      9                A1               RST             Orange
 * SPI SS     10               A2               SDA             Blue
 * SPI MOSI   11               MO/D12           MOSI            Green
 * SPI MISO   12               MI/D11           MISO            White
 * SPI SCK    13               SCK/D13	        SCK             Yellow
 * PWR                         3.3V                             Red
 * GND                         GND                              Black
 *
 * See readme for parts and assembly.
 *
 * Pressing the mode button will trigger the device into sleeping.  Wake it up
 with reset button.
 */

#define SS_PIN A2
#define RST_PIN A1
#define LED_PIN D7
#define BUZZER_PIN D8
#define INVALID_TIME 0
#define NDEF_MFRC522_PARTICLE
int version = 10;

MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance.
bool justSetup = false;           // used to send report on the first loop call
bool sleeping = false;            // helps communicate when goinging into and out of sleep
bool goToSleepTrigger = false;    // manual trigger for sleep mode
LEDStatus scanningIndicator(RGB_COLOR_YELLOW, LED_PATTERN_SOLID,
                            LED_SPEED_NORMAL, LED_PRIORITY_CRITICAL);
LEDStatus duplicateScanIndicator(scanningIndicator.color(), LED_PATTERN_BLINK,
                                 LED_SPEED_NORMAL, LED_PRIORITY_IMPORTANT);
LEDStatus goingToSleepIndicator(RGB_COLOR_GRAY, LED_PATTERN_FADE,
                                LED_SPEED_FAST, LED_PRIORITY_IMPORTANT);
LEDStatus wakingUpIndicator(RGB_COLOR_ORANGE, LED_PATTERN_BLINK, LED_SPEED_FAST,
                            LED_PRIORITY_IMPORTANT);

// allows for publish and forget
// https://community.particle.io/t/particle-publish-and-blocking/42022
retained uint8_t publishQueueRetainedBuffer[2048];
PublishQueueAsync publishQueue(publishQueueRetainedBuffer,
                               sizeof(publishQueueRetainedBuffer));

// for going to sleep when not in use
unsigned long mostRecentScanMillis = 0;
int scanCount = 0; // game stats indicating all scan attempts
int duplicateScanCount =
    0; // a subset of scanCount indicating repeat scan of the same tag

// wake up for morning (7-9) and --afternoon (14-16) commutes--> too much power
// consumption
// notice periods are sleep periods...not wake (9-14),(16-tomorrow at7)
// if it awakes before 9, it will remain awake unless manually told to sleep
int sleepPeriods[] = {
    9, 24 + 7};           // sleepHour,wakeHour,sleepHour,wakeHour tomorrow,etc
int sleepPeriodCount = 1; // corresponds to sleepPeriods tuples

SYSTEM_THREAD(ENABLED); // run networking in the background so application runs
                        // without blocking
TagIo io(mfrc522);
StationSound sound(BUZZER_PIN);

void setup()
{
  Particle.variable("appVersion", version);
  Particle.variable("scanCount", scanCount);
  Particle.variable("scanCountDup", duplicateScanCount);
  Particle.function("voltage", publishVoltage);
  Particle.function("sleep", sleepTime);
  pinMode(RST_PIN, OUTPUT); // used to power down the MFRC522
  pinMode(LED_PIN, OUTPUT); // provides feedback that a scan is taking place
  pinMode(BATT, INPUT);     // checks voltage

  mfrc522.setSPIConfig();

  mfrc522.PCD_Init(); // Init MFRC522 card
  justSetup = true;

  // publishes vital stats like voltage, etc.
  Particle.subscribe("vitals", vitalsHandler, MY_DEVICES);

  Particle.subscribe("requiredVersion", requiredVersionHandler, MY_DEVICES);
  // set to the local time zone for time reporting in scans
  Time.zone(-7.0); //TODO:subscribe for this

  System.on(button_click,
            [](system_event_t ev, int clicks) -> void { sleepTime("button"); });
}

void loop()
{
  reportSetupStatus();
  readNfcTag();
  lookForBedtime();
}

/**
 * Reads new NFC tags, provides feedback to the user and publishes the UID to
 * the cloud.
 */
void readNfcTag()
{
  if (io.tagIsReady())
  {
    if (!io.tagIsNew())
    {
      duplicateScanDetected();
      return;
    }
  }
  else
  {
    return;
  }
  mostRecentScanMillis = millis();
  scanCount++;

  digitalWrite(LED_PIN, HIGH);
  String uid = io.uid();

  scanningIndicator.setActive(true);
  String json = visitsJson(uid);

  //FIXME: stop printing and start sending
  io.print();
  Serial.println(json);

  prepareTagForNextDay(uid);

  digitalWrite(LED_PIN, LOW);
  scanningIndicator.setActive(false);

  // WITH_ACK is important to ensure delivery
  publishQueue.publish("stations-scans", json, PRIVATE, WITH_ACK);
  //TODO: use an async tone during the entire read/write
  //this would be the sound of successful session after prepare for next day
  sound.ofSuccessfulVisit(io.numberOfVisits() + 1); //+1 for this visit
}

/**
 * Builds a JSON array of scanJson objects to be sent to the cloud for rewarding.
 * @return array of scanJson objects. 1 for every visit
 */
String visitsJson(String uid)
{
  String json = String("[\n");

  int numberOfVisits = io.numberOfVisits();
  for (int i = 0; i < numberOfVisits; i++)
  {
    TagIo::Visit visit = io.readVisit(i);
    String scan = scanJson(uid, String(visit.stationId), visit.timestamp);
    json.concat(scan.c_str());
    json.concat(",\n");
  }

  //one more scan from this station
  {
    String stationId = System.deviceID(); // always available and never changes
    time_t timestamp;
    if (isValidTime())
    {
      timestamp = Time.now();
    }
    else
    {
      timestamp = INVALID_TIME;
    }
    json.concat(scanJson(uid, stationId, timestamp));
  }

  json.concat("\n]");
  return json;
}
/**
 * builds the json object for a single scan.
 */
String scanJson(String uid, String stationId, time_t timestamp)
{
  // the time of the scan
  String json = String::format("{\"tagId\":\"%s\",\"station\":\"%s\"",
                               uid.c_str(), stationId.c_str());

  if (timestamp != INVALID_TIME)
  {
    String isoTimestamp = Time.format(timestamp, TIME_FORMAT_ISO8601_FULL);
    json.concat(String::format(",\"timestamp\":\"%s\"", isoTimestamp.c_str()));
  }

  json.concat("}");
  return json;
}

/**
 * Time is acquired after network connection, otherwise 1999 is provided.  Make
 * sure time is updated.
 */
bool isValidTime() { return Time.year() > 2018; }

/**
 */
bool inactive()
{
  return millis() - mostRecentScanMillis > 300000; // 5 min * 60 sec * 1000 ms;
}

/**
 * The device must power down to save power for sleep mode.
 */
void lookForBedtime()
{
  // avoid checking if already sleeping, but somehow ended up back in the loop
  // if sleep triggered, then goes to sleep immediately
  // only check once per minute
  // don't bother looking until inactivity has been detected
  // invalid time will put device to sleep based on the wrong time of day and
  // won't wake up on time
  if (goToSleepTrigger ||
      (isValidTime() && inactive() && Time.second() % 50 == 0))
  {

    int i = 0;
    // determine if now is between sleep time and wake time (i.e. bedtime)
    int nowHour = Time.hour();

    // multiple periods in a day are considered
    while (i < sleepPeriodCount)
    {
      int groupIndex = 2 * i;
      int sleepHour = sleepPeriods[groupIndex];
      int wakeHour = sleepPeriods[groupIndex + 1];
      // can only go to sleep if the time to papwake is in the future
      if (nowHour < wakeHour)
      {
        bool afterBedtime = nowHour >= sleepHour;
        // goToSleep is a manual request that should always be respected
        // any time after bedtime, we should sleep unless the reset button was
        // pressed (maintenance, testing, etc)
        if (afterBedtime || goToSleepTrigger)
        {
          goToSleep(wakeHour);
          return; // avoid checking future times which may call wake up
        }
        else
        {
          wakeUp();
        }
      }
      i++;
    }
  }
}

/**
 * When a scan is duplicated, this will communicate such to the player and the system. 
 */
void duplicateScanDetected()
{
  sound.ofRepeatTag();
  // notify the user we already got the scan
  duplicateScanCount++;
  duplicateScanIndicator.setActive(true);
  for (int i = 0; i < 5; i++)
  {
    delay(100);
    digitalWrite(LED_PIN, HIGH);
    delay(100);
    digitalWrite(LED_PIN, LOW);
  }
  duplicateScanIndicator.setActive(false);
}
/**
 * If not already sleeping, this will go to sleep by first shutting down the NFC
 * reader and then going into a particle sleep. The timer still works so the
 * wake time will re-activate. The sleeping boolean is persistent during sleep
 * which leads us to this check.
 */
void goToSleep(int wakeUpHour)
{
  if (!sleeping)
  {
    sleeping = true;
    goToSleepTrigger = false;

    // if wake up tomorrow, extend til monday if weekend
    if (wakeUpHour > 24)
    {
      int weekday = Time.weekday();
      // Friday should sleep through 2 days (sat + sun)
      if (weekday == 6)
      {
        wakeUpHour += 48;
      }
      else if (weekday == 7)
      {
        // unsual, but if awake on saturday, sleep until monday
        wakeUpHour += 24;
      } // else every other day, including sunday, wake up tomorrow
    }
    // calculate the seconds til wake
    int secondsInHour = 3600;
    int secondsPastHour = Time.minute() * 60;
    int hoursTilWakeup = wakeUpHour - Time.hour();
    int secondsTilWakeupHour = hoursTilWakeup * secondsInHour;
    int secondsTilWakeUp = secondsTilWakeupHour -
                           secondsPastHour; // ensures wake up is on the hour

    publishQueue.publish(
        "sleep",
        String::format("Going to sleep for %i seconds...", secondsTilWakeUp),
        PRIVATE);

    // send a voltage update before sleep
    publishVoltage("sleeping");

    // turn off the RFID board to save power
    // https://www.nxp.com/docs/en/data-sheet/MFRC522.pdf
    // see 8.6.1 Hard power-down
    digitalWrite(RST_PIN, LOW);

    // give several seconds of notice that sleep is coming
    goingToSleepIndicator.setActive(true);
    delay(5000);
    goingToSleepIndicator.setActive(false);

    System.sleep({D0}, FALLING,
                 secondsTilWakeUp); // D0 is a dud because empty has mystery
                                    // wakeups.  May be related?
  }
}

/**
 * Wakes up after a sleep, only if sleeping.  Will publish a notice and restart
 * to also wakeup the RFID board.
 */
void wakeUp()
{
  if (sleeping)
  {
    // report waking up and reset for a fresh start
    if (System.wokenUpByRtc())
    {
      publishQueue.publish("wakeup", "Woken up after scheduled sleep.",
                           PRIVATE);
    }
    else if (System.wokenUpByPin())
    {
      publishQueue.publish("wakeup", "Woken up by pin (TBD which pin?).",
                           PRIVATE);
    }
    else
    {
      publishQueue.publish("wakeup", "Woken up for unknown reason.", PRIVATE);
    }

    // communicates that the reset is coming since it just woke up
    wakingUpIndicator.setActive(true);
    delay(5000);
    // it's a brand new day...start fresh. Required for MFRC522 Board to wake up
    System.reset();
  }
}
/**
 * When initialized (justSetup variable), this does a quick status check and
 * reports to the cloud. Some things don't work in the setup so must be done in
 * the loop (MFRC522 status?).
 */
void reportSetupStatus()
{
  if (justSetup)
  {
    updateNfcReady();
    publishVoltage("setup");
    justSetup = false;
  }
}

/**
 * handler for a subscription to publish voltage and other vitals on request.
 * Also useful to find out who is alive.
 */
void vitalsHandler(const char *topic, const char *data)
{
  publishVoltage("vitals");
  updateNfcReady();
}

/**Given a version to seek, this will publish a response only if the required
 * version is not the same as the version of this code.
 */
void requiredVersionHandler(const char *topic, const char *data)
{
  String versionString = String::format("%i", version);
  if (versionString != String(data))
  {
    publishVersion();
  }
}
void publishVersion()
{
  String message = String::format("%i", version);
  publishQueue.publish("version", message, PRIVATE);
}
/* Checks the voltage on a periodic basis and publishes an event for remote
 * monitoring.
 * @param source indicates who requested this and is sent with the message.
 */
int publishVoltage(String source)
{
  String message =
      String::format("%s volts during %s", voltage().c_str(), source.c_str());
  return publishQueue.publish("voltage", message, PRIVATE);
}

/**
 * returns the voltage with 2 decimal places.
 */
String voltage()
{
  double value = analogRead(BATT) * 0.0011224;
  return String::format("%.2f", value);
}

/** Checks the MFRC522 RST pin to make sure it is high...which indicates it is
 * ready to read.*/
void updateNfcReady()
{
  int ready = digitalRead(RST_PIN);
  if (ready == HIGH)
  {
    String systemVersion = System.version();
    publishQueue.publish("NFC",
                         String::format("v%i on OS v%s ready to scan.", version,
                                        systemVersion.c_str()),
                         PRIVATE);
  }
  else
  {
    publishQueue.publish("NFC", "ERROR: Card reader is not ready.", PRIVATE);
  }
}

/**
 * Sets a boolean that will invoke sleep until the next school day.
 * setup for remote function
 */
int sleepTime(String extra)
{
  goToSleepTrigger = true;
  return 0;
}

/**
 * Wipes the tag clean and writes a url to the player's tag so an NFC enabled phone will open to the player's 
 * scoreboard.
 * @uid used for referencing the tag in the url
 * @schoolCode  the school where this station is located used as the subdomain of schooltag.org
 * @return true if write is a success
 */
bool prepareTagForNextDay(String uid)
{
  ndef_mfrc522::NdefMessage message = ndef_mfrc522::NdefMessage();
  String url = String("https://schooltag.org?tag=");
  url.concat(uid);
  message.addUriRecord(url);
  MifareUltralight writer = MifareUltralight(mfrc522);
  writer.clean();
  return writer.write(message);
}